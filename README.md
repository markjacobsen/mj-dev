# README #

This repository is intended to simplify creating consistent development environment.
To get started, it is assumed you have installed [Git for Windows](https://gitforwindows.org/)

### What is this repository for? ###

* Create a consistent development enviroment
* 1.0
* [Learn More](https://markjacobsen.net/my-projects/)

### How do I get set up? ###

* Download and install [Git for Windows](https://gitforwindows.org/)
* From the "Git Bash" shell provided run the following commands:

```
mkdir $HOME/dev
mkdir $HOME/dev/git
git clone https://markjacobsen@bitbucket.org/markjacobsen/mj-dev.git $HOME/dev/git/mj-dev
$HOME/dev/git/mj-dev/setup.sh
```