echo $HOME

# START: User Prefs ===========================================
# You do not NEED to modify these values, but if you do, make 
# sure they are the equivalent of each other (one using *nix 
# pathing and the other Windows)
# -------------------------------------------------------------
export WORK_DIR=$HOME
export WORK_DIR_WIN=$USERPROFILE
# END: User Prefs =============================================

cd $WORK_DIR

mkdir $WORK_DIR/dev
mkdir $WORK_DIR/dev/downloads
mkdir $WORK_DIR/dev/lib
mkdir $WORK_DIR/dev/git
mkdir $WORK_DIR/dev/bin
mkdir $WORK_DIR/dev/eclipse-workspace


cd $WORK_DIR/dev/downloads

if [ ! -f "apache-maven-3.6.3-bin.zip" ]; then
	curl -O https://downloads.apache.org/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip
	unzip -n $WORK_DIR/dev/downloads/apache-maven-3.6.3-bin.zip -d $WORK_DIR/dev/lib
	setx MAVEN_HOME "$WORK_DIR_WIN\dev\lib\apache-maven-3.6.3"
else
	echo "Maven download exists"
fi

if [ ! -f "openjdk-13.0.2_windows-x64_bin.zip" ]; then
	curl -O https://download.java.net/java/GA/jdk13.0.2/d4173c853231432d94f001e99d882ca7/8/GPL/openjdk-13.0.2_windows-x64_bin.zip
	unzip -n $WORK_DIR/dev/downloads/openjdk-13.0.2_windows-x64_bin.zip -d $WORK_DIR/dev/bin
	setx JAVA_HOME "$WORK_DIR_WIN\dev\bin\jdk-13.0.2"
else
	echo "Open JDK download exists"
fi

if [ ! -f "eclipse-jee-2019-12-R-win32-x86_64.zip" ]; then
	curl -O http://ftp.osuosl.org/pub/eclipse/technology/epp/downloads/release/2019-12/R/eclipse-jee-2019-12-R-win32-x86_64.zip
	unzip -n $WORK_DIR/dev/downloads/eclipse-jee-2019-12-R-win32-x86_64.zip -d $WORK_DIR/dev/bin
else
	echo "Eclipse download exists"
fi

setx PATH "%MAVEN_HOME%\bin;%JAVA_HOME%\bin;%PATH%"

